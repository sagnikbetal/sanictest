from app import app

if __name__ == "__main__":
    is_dev = False
    if app.config.ENV == "dev":
        is_dev = True
    app.run(host="0.0.0.0", port=app.config.PORT, dev=is_dev)
