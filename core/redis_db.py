import redis
from sanic.log import logger
from json import dumps


class RedisDB:
    connection: redis.Redis = None

    def __init__(self):
        pass

    @classmethod
    def connect(cls, db_string: str = None):
        if cls.connection is not None:
            logger.info("Redis already connected")
            return cls.connection
        logger.info("Redis connected")
        cls.connection = redis.from_url(db_string, db=0, decode_responses=True)
        cls.connection.ping()
        return cls.connection

    @classmethod
    def disconnect(cls):
        cls.connection.flushdb()
        cls.connection.close()
        cls.connection = None

    def store_data_with_expiry(self, key: str, data, expiry_time_in_sec: int):
        storage_value = dumps(data) if type(data) is dict else data
        result = self.connection.setex(name=key, time=expiry_time_in_sec, value=storage_value)
        if not result:
            err = "could not store value in redis"
            logger.error("[store_data_with_expiry] {}".format(err))
            raise Exception(err)
        return True

    def get_data(self, key: str):
        return self.connection.get(key)
