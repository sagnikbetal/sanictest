from sqlalchemy import (Column, String, Float,
                        DateTime, Boolean, ForeignKey,
                        UniqueConstraint
                        )
from sqlalchemy.orm import declarative_base, relationship
from datetime import datetime
from core.db import Database

Base = declarative_base()


# Category and product has a one-to-many relationship
class Categories(Base):
    __tablename__ = "categories"
    id = Column(String(40), primary_key=True)
    title = Column(String(32), nullable=False, unique=True)
    is_active = Column(Boolean, default=True)
    date_added = Column(DateTime, default=datetime.utcnow())
    date_updated = Column(DateTime, default=datetime.utcnow(), onupdate=datetime.utcnow())
    product_relation = relationship("Products", backref="categories")

    def __repr__(self):
        return "categories"


# TODO: create composite primary key, manually
class Products(Base):
    __tablename__ = "products"
    id = Column(String(40), primary_key=True)
    title = Column(String(32), nullable=False, unique=True)
    category_id = Column(String(40), ForeignKey('categories.id'), nullable=False)
    price = Column(Float, default=0)
    is_active = Column(Boolean, default=True)
    date_added = Column(DateTime, default=datetime.utcnow())
    date_updated = Column(DateTime, default=datetime.utcnow(), onupdate=datetime.utcnow())
    UniqueConstraint("category_id", "title", name="product_check")

    def __repr__(self):
        return "products"

# Run once - this will create the tables in the DB
# Base.metadata.create_all(Database().connect(db_string=""))
