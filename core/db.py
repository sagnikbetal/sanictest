from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import IntegrityError
from sanic.exceptions import SanicException
from sanic.log import logger


class Database:
    db = None
    session = None
    # TODO add a method to check connection is successful
    @classmethod
    def connect(cls, db_string=None, echo=False):
        if cls.db is not None:
            return cls.db
        cls.db = create_engine(db_string, echo=echo)
        logger.info("DB connected -> {}".format(cls.db.pool.status()))
        return cls.db

    @classmethod
    def manage_session(cls):
        if cls.session is not None:
            return cls.session
        s = sessionmaker(bind=cls.connect())
        session = s()
        return session

    def get_all_records(self, model):
        logger.info("Getting all records: {}".format(model))
        session = self.manage_session()

        records = session.query(model).filter_by(is_active=True).order_by(model.date_updated.desc()).all()
        return records

    def get_one_record(self, model, **filters):
        logger.info("Getting one record: {}".format(model))
        session = self.manage_session()

        record = session.query(model).filter_by(is_active=True).filter_by(**filters).first()
        if record is None:
            return False

        return record

    def insert_record(self, new_obj):
        logger.info("Inserting record")
        session = self.manage_session()

        try:
            session.add(new_obj)
            session.commit()
            return "Added"

        except IntegrityError as e:
            session.rollback()
            raise SanicException(str(e), status_code=400)

    def update_one(self, model, filter_params, update_params):
        logger.info("Updating record")
        session = self.manage_session()

        old_record = session.query(model).filter(
            and_(model.id == filter_params.get("id"), model.is_active == True)).first()
        if old_record is None:
            return False
        old_record.title = update_params.get("title").lower()
        session.commit()
        return "Updated"

    # use at own risk
    def delete_one(self, model, filter_params):
        logger.info("deleting record")
        session = self.manage_session()

        old_record = session.query(model).filter(model.id == filter_params.get("id")).first()
        session.delete(old_record)
        session.commit()
        return "Deleted"

    def delete_one_soft(self, model, filter_params):
        logger.info("soft deleting record")
        session = self.manage_session()

        old_record = session.query(model).filter(
            and_(model.id == filter_params.get("id"), model.is_active == True)).first()
        if old_record is None:
            return False
        old_record.is_active = False
        session.commit()
        return "Soft Deleted"

    def join(self, model1, model2):
        logger.info("Getting data via join")
        session = self.manage_session()

        result = session.query(model1).join(model2) \
            .filter(and_(model1.is_active == True, model2.is_active == True, )) \
            .order_by(model2.id.desc()).all()
        return result
