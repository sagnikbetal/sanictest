from sanic import Sanic, response, exceptions
from sanic.exceptions import NotFound, ServerError
from core.db import Database
from core.redis_db import RedisDB
from main_routes import base_bp_group, home_bp

app = Sanic("test_app")
app.update_config("./env.py")


@app.listener("before_server_start")
async def setup_db(app):
    app.ctx.db = Database().connect(db_string=app.config.SQL_DB_STRING,
                                    echo=True if app.config.ENV == "dev" else False)
    app.ctx.redis = RedisDB()
    app.ctx.redis.connect(db_string=app.config.REDIS_DB_STRING)


@app.listener('after_server_stop')
async def housekeeping(app):
    pass  # app.ctx.redis.disconnect()


# TODO : not working as expected
@app.exception(ServerError)
async def handle_500(req, exception):
    return response.json({"url": req.url, "method": req.method, "msg": exception.message}, status=401)


@app.exception(NotFound)
async def handle_404(req, exception):
    return response.json({"url": req.url, "method": req.method, "msg": "page not found"}, status=404)


# app.static('/public/page_not_found', './public/html/404.html')
# app.static('/public/images/dp', './public/images/Snapseed.jpg')

app.blueprint([home_bp, base_bp_group])
