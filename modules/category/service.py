# import sys
# sys.path.append("/Users/sagnikbetal/Desktop/myProjects/Pylearn/Frame")
from sanic.log import logger
from core.models import Categories, Products
from core.redis_db import RedisDB
from core.db import Database
import uuid
from json import loads

CATEGORY_WITH_PRODUCTS_REDIS_KEY: str = "CATEGORY_WITH_PRODUCTS"
CATEGORY_WITH_PRODUCTS_REDIS_VALUE_EXPIRY_SEC = 60 * 1


# todo add pagination,filter
def get_categories():
    list_result = []
    result = Database().get_all_records(Categories)
    for r in result:
        list_result.append({
            "title": r.title,
            "id": str(r.id),
            "date_added": r.date_added.strftime("%c"),
            "date_updated": r.date_updated.strftime("%c")
        })
    return list_result


def get_one_category(**filter_params):
    category = Database().get_one_record(Categories, **filter_params)
    if not category:
        return {}

    return {
        "title": category.title,
        "id": str(category.id),
        "date_added": category.date_added.strftime("%c"),
        "date_updated": category.date_updated.strftime("%c"),
        "is_active": category.is_active
    }


# check data in cache, if true return else fallback
def check_in_cache_category_with_products():
    logger.info("Checking category_with_products in cache")
    cached_data = RedisDB().get_data(CATEGORY_WITH_PRODUCTS_REDIS_KEY)
    if cached_data:
        logger.info("category_with_products exists")
        return loads(cached_data)
    return False


def store_in_cache_category_with_products():
    data_from_db = get_category_with_products()
    logger.info("Storing category_with_products in cache")
    d = RedisDB().store_data_with_expiry(CATEGORY_WITH_PRODUCTS_REDIS_KEY,
                                         data_from_db, CATEGORY_WITH_PRODUCTS_REDIS_VALUE_EXPIRY_SEC)
    return data_from_db


def get_category_with_products():
    result = []
    product_records = Database().join(Products, Categories)
    for product in product_records:
        result.append({
            "category_title": product.categories.title,
            "category_id": product.categories.id,
            "category_date_added": product.categories.date_added.strftime("%c"),
            "title": product.title,
            "id": product.id,
            "price": product.price
        })

    # group & format
    old_category_id = None
    category_index = -1
    data = {
        "total_products": len(result),
        "categories": []
    }

    for r in result:

        product = {
            "id": r["id"],
            "title": r["title"],
            "price": r["price"]
        }

        # since it is sorted, we can aggregate easily
        if old_category_id != r["category_id"]:
            # update values
            old_category_id = r["category_id"]
            category_index += 1

            category = {
                "name": r["category_title"],
                "id": r["category_id"],
                "products": [product]
            }
            data["categories"].append(category)

        else:
            data["categories"][category_index]["products"].append(product)
    return data


def add_category(title: str):
    new_category = Categories(id="cat_{}".format(uuid.uuid4()), title=title.lower())
    status = Database().insert_record(new_category)
    return status


def update_category(filter_params: dict, update_params: dict):
    status = Database().update_one(Categories, filter_params, update_params)
    return status


def delete_category(filter_params: dict):
    status = Database().delete_one_soft(Categories, filter_params)
    return status
