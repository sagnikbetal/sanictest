from sanic import Blueprint
from sanic.response import text

from modules.category.controller import Category, CategoryWithId, CategoryWiseProducts

category_blueprint = Blueprint("category_blueprint", url_prefix="/categories")

# @category_blueprint.get("/test")
# async def test(res):
#     return text("Hello world test")

category_blueprint.add_route(Category.as_view(), "/")
category_blueprint.add_route(CategoryWithId.as_view(), "/<id:str>")
category_blueprint.add_route(CategoryWiseProducts.as_view(), "/products")

