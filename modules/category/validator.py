import attrs


@attrs.define
class add_category_validation:
    title: str


# TODO : try nested validation
@attrs.define
class update_category_validation:
    update: dict
