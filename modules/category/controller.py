# category controller

from sanic.views import HTTPMethodView
from sanic.exceptions import SanicException
from sanic import response
from sanic_ext import validate
from modules.category.service import (get_categories, get_one_category, add_category,
                                      update_category, delete_category,
                                      check_in_cache_category_with_products, store_in_cache_category_with_products)
from modules.category.validator import add_category_validation, update_category_validation
import attrs


class Category(HTTPMethodView):
    async def get(self, req):
        data = get_categories()
        return response.json({"data": data})

    @validate(json=add_category_validation)
    async def post(self, req, body):
        params = attrs.asdict(body)
        status = add_category(title=params["title"])
        return response.json({"status": status}, status=201)


class CategoryWithId(HTTPMethodView):

    async def get(self, req, id):
        data = get_one_category(id=id)
        return response.json({"data": [data]})

    @validate(json=update_category_validation)
    async def patch(self, req, id, body):

        filter_params = {"id": id}
        update_params = attrs.asdict(body)["update"]

        if not filter_params.get("id") or not update_params.get("title"):
            raise SanicException("Param missing", status_code=400)
        status = update_category(filter_params, update_params)
        return response.json({"status": status})

    async def delete(self, req, id):
        filter_params = {"id": id}
        if not filter_params.get("id"):
            raise SanicException("Param missing !", status_code=400)
        status = delete_category(filter_params)
        return response.json({"status": status})


class CategoryWiseProducts(HTTPMethodView):

    async def get(self, req):
        data = check_in_cache_category_with_products()
        if not data:
            data = store_in_cache_category_with_products()
        return response.json({"data": data})
