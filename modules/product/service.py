# import sys
# sys.path.append("/Users/sagnikbetal/Desktop/myProjects/Pylearn/Frame")

from core.models import Products
from core.db import Database
import uuid


def get_products():
    list_result = []
    result = Database().get_all_records(Products)
    for r in result:
        list_result.append({
            "id": str(r.id),
            "title": r.title,
            "category_id": str(r.category_id),
            "price": r.price,
            "is_active": r.is_active,
            "date_added": r.date_added.strftime("%c")
        })
    return list_result


def get_one_product(**filter_params):
    product = Database().get_one_record(Products, **filter_params)
    if not product:
        return {}
    return {
        "title": product.title,
        "id": str(product.id),
        "price": product.price,
        "date_added": product.date_added.strftime("%c"),
        "date_updated": product.date_updated.strftime("%c"),
        "is_active": product.is_active
    }


def add_product(title: str, category_id: str, price: float):
    new_product = Products(id="prd_{}".format(uuid.uuid4()), title=title.lower(), category_id=category_id, price=price)
    status = Database().insert_record(new_product)
    return status


def delete_product(filter_params: dict):
    status = Database().delete_one_soft(Products, filter_params)
    return status
