from sanic.views import HTTPMethodView
from sanic.exceptions import SanicException
from sanic import response
from sanic_ext import validate

from modules.product.service import add_product,get_products, delete_product, get_one_product
from modules.product.validator import add_product_validation
import attrs


class Product(HTTPMethodView):
    async def get(self, req):
        data = get_products()
        return response.json({"data": data})

    @validate(json=add_product_validation)
    async def post(self, req,body):

        params = attrs.asdict(body)
        status = add_product(title=params.get("title"),
                             category_id=params.get("category_id"),
                             price=params.get("price")
                             )
        return response.json({"status": status}, status=201)


class ProductWithId(HTTPMethodView):

    async def get(self, req, id):
        data = get_one_product(id=id)
        return response.json({"data": [data]})

    async def delete(self,req,id):
        filter_params = {"id": id}
        if not filter_params.get("id"):
            raise SanicException("Param missing !", status_code=400)
        status = delete_product(filter_params)
        return response.json({"status": status})
