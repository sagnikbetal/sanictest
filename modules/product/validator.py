import attrs

@attrs.define
class add_product_validation:
    title:str
    category_id:str
    price:float