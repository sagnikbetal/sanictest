from sanic import Blueprint
from modules.product.controller import Product,ProductWithId

product_blueprint = Blueprint("product_blueprint", url_prefix="/products")

product_blueprint.add_route(Product.as_view(), "/")
product_blueprint.add_route(ProductWithId.as_view(), "/<id:str>")
