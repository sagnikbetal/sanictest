from sanic.response import file, text
from sanic import Blueprint
from modules.category.route import category_blueprint
from modules.product.route import product_blueprint

home_bp = Blueprint("home", url_prefix="/")


@home_bp.route(uri="/", methods=["GET"])
async def home_route(req):
    return text("Server running")


# api routes
base_bp_group = Blueprint.group(
    product_blueprint,
    category_blueprint, url_prefix="/v1")
