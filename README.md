# 🏃🏽 Sanic Hello World 

This is a basic Python/Sanic web application with a SQL Database connection,
caching and message broker implemented

### Tech Stack: 
 * PgSQL with SqlAlchemy ORM
 * Redis
 * Kafka

### Goals
1. Create entities ( Category, Product ) having a one-to-many relationship
2. RESTful CRUD apis with basic param validation 
3. Creating a layered repo structure 
4. Implement caching on an endpoint
5. Use events to invalidate and update cache

## Setup:
1. Configure *env.txt* in root directory and rename it to *env.py* 
2. Setup PostgreSQL, Redis, Kafka locally
3. install modules from *requirements.txt*
4. open terminal navigate to project directory run "python server.py"
5. open browser and visit http://localhost:8080/
6. try out the api by importing the POSTMAN collection from root directory 


---
Note: **GET /v1/categories/products** is the api where caching is implemented,
will get invalidated if there are changes in product/categories via the other apis

